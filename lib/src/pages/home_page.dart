import 'package:componentes/src/pages/alert_page.dart';
import 'package:componentes/src/providers/menu_provider.dart';
import 'package:componentes/src/utils/icono_string_util.dart';
import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Componentes'),
        centerTitle: true,
        backgroundColor: Colors.black,
      ),
      body: _lista(),
      
      
    );
  }

  
  Widget _lista() {

    //print(menuProvider.opciones);
   /*  menuProvider.cargarData().then((opciones){
      print('_lista');
      print(opciones);
    }); */



    return FutureBuilder(
      future: menuProvider.cargarData(),
      initialData: [],
      builder: ( context, AsyncSnapshot<List<dynamic>> snapshot) {

        

        return ListView(
          children: _listItems(snapshot.data, context),
          
        );


      },
    );
  /*   return ListView(
      children: _listItems(),
    ); */

  }

 List<Widget> _listItems( List<dynamic> data, BuildContext context ) {
   
   final List<Widget> opciones = [];

   //if ( data == null){ return []; }


   data.forEach((opt){
     final widgetTemp = ListTile(
      title: Text(opt['texto']),
      leading: getIcon(opt['icon']),
      trailing: Icon( Icons.arrow_forward,  color: Colors.black),
      onTap: () {

        Navigator.pushNamed(context, opt['ruta']);

        /* final route = MaterialPageRoute(
          builder: ( context ) {
            return AlertPage();
          } 
        );

        Navigator.push(context, route);
 */
      },

     );

     opciones.add(widgetTemp);
     opciones.add(Divider());

   });

   return opciones;
 }
}