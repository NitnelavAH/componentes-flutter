import 'package:flutter/material.dart';

class CardPage extends StatelessWidget {
  const CardPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Cards'),
        centerTitle: true,
        backgroundColor: Colors.black,
      ),
      body: ListView(
        padding: EdgeInsets.all(10.0),
        children: <Widget>[
          _cardTipo1(),
          SizedBox(height: 30.0),
          _cardTipo2()
        ],
      ),
    );
  }

 Widget _cardTipo1() {

   return Card(
     child: Column(
       children: <Widget>[
         ListTile(
           leading: Icon(Icons.photo_album, color: Colors.blueAccent),
           title: Text('titulo'),
           subtitle: Text('subtitulo de la tarjeta en componentes flutter proyecto del curso de Fernando Herrera'),
           
         ),
         Row(
           children: <Widget>[
             FlatButton(onPressed: () {}, child: Text('Cancelar')),
             FlatButton(onPressed: () {}, child: Text('Ok')),
           ],
           mainAxisAlignment: MainAxisAlignment.end,
         )
       ],
     ),
   );
 }

  _cardTipo2() {

    return Card(
      child: Column(
        children: <Widget>[
          FadeInImage(
            placeholder: AssetImage('assets/jar-loading.gif'), 
            image: NetworkImage('https://upload.wikimedia.org/wikipedia/commons/a/ae/Joseph_Wright_of_Derby_-_Italian_Landscape_with_Mountains_and_a_River_-_Google_Art_Project.jpg'),
            fadeInDuration: Duration(milliseconds: 200),
            height: 300.0,
            fit: BoxFit.cover,
          ),
        /*   Image(
            image: NetworkImage('https://upload.wikimedia.org/wikipedia/commons/a/ae/Joseph_Wright_of_Derby_-_Italian_Landscape_with_Mountains_and_a_River_-_Google_Art_Project.jpg')
          ), */
          Container(
            padding: EdgeInsets.all(10.0),
            child: Text('Imagen de prueba de card')
          )
        ],
      ),
    );
  }
}